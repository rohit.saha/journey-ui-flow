import * as React from "react";
import * as _ from "lodash";
import { TrayWidget } from "./TrayWidget";
import { Tray } from "./TrayWidget";
import { Application } from "./Application";
import { TrayItemWidget } from "./TrayItemWidget";
import QueryBuilder from "./QueryBuilder/App"
import {
  DefaultNodeModel,
  LinkModel,
  NodeModel,
} from "@projectstorm/react-diagrams";
import {
  BaseModel,
  CanvasWidget,
  DeleteItemsAction,
} from "@projectstorm/react-canvas-core";




import styled from "@emotion/styled";
import beautify from "json-beautify";
import { stringify } from "querystring";
import * as dataforPages from "./json/screenNew.json";
import FlavorForm from "./components/MyForm"
import ApiForm from "./components/ApiForm"

export interface BodyWidgetProps {
  app: Application;
}

export const Container = styled.div<{ color: string; background: string }>`
  height: 100%;
  background-color: ${(p) => p.background};
  background-size: 50px 50px;
  display: flex;

  > * {
    height: 100%;
    min-height: 100%;
    width: 100%;
  }
  background-image: linear-gradient(
      0deg,
      transparent 24%,
      ${(p) => p.color} 25%,
      ${(p) => p.color} 26%,
      transparent 27%,
      transparent 74%,
      ${(p) => p.color} 75%,
      ${(p) => p.color} 76%,
      transparent 77%,
      transparent
    ),
    linear-gradient(
      90deg,
      transparent 24%,
      ${(p) => p.color} 25%,
      ${(p) => p.color} 26%,
      transparent 27%,
      transparent 74%,
      ${(p) => p.color} 75%,
      ${(p) => p.color} 76%,
      transparent 77%,
      transparent
    );
`;

export const Body = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  min-height: 100%;
`;

export const Header = styled.div`
  display: flex;
  background: rgb(30, 30, 30);
  flex-grow: 0;
  flex-shrink: 0;
  color: white;
  font-family: Helvetica, Arial, sans-serif;
  padding: 10px;
  align-items: center;
`;

export const Content = styled.div`
  display: flex;
  flex-grow: 1;
`;

export const Layer = styled.div`
  position: relative;
  flex-grow: 1;
`;

export const Toolbar = styled.div`
  /* padding: 5px; */
  background: rgb(21, 4, 179);
  display: flex;
  flex-shrink: 0;
`;

export const DemoButton = styled.button`
  background: rgb(60, 60, 60);
  font-size: 14px;
  padding: 5px 10px;
  border: none;
  color: white;
  outline: none;
  cursor: pointer;
  margin: 2px;
  border-radius: 3px;

  &:hover {
    background: rgb(0, 192, 255);
  }
`;


//load json from json folder

let text=JSON.stringify(dataforPages);
console.log(JSON.stringify(JSON.parse(text)));

const dataFromApi = [
  {
    name: "nsdl",
    url: "http://something",
    version: "1.0",
    requestbody: {
      pan_id: "source",
      mobile_number: "source",
      vendor_id: "source",
      request_id: "source",
    },
    header: {
      Authorization: "source",
      "Content-Type": "application/json",
    },
    method: "HTTP methods",
    response: {
      "200": {
        status: "number",
        data: {
          "PAN-title": "string",
          "Middle-Name": "string",
          "Last-Name": "string",
          "PAN Id": "string",
          "PAN-Status": "string",
          "First-Name": "string",
          "Status code": "string",
          "Last-update-date": "date",
        },
        description: "string",
      },
      "400": {
        status: "number",
        description: "string",
      },
      "500": {
        status: "number",
        description: "string",
      },
    },
  },
  {
    name: "name_match",
    url: "http://something",
    version: "1.0",
    requestbody: {
      Product: "source",
      "Loan Application Id": "source",
      "Transaction Id": "source",
      "PAN Name": "source",
      "NSDL Name": "source",
    },
    header: {
      "x-api-key": "source",
      "Content-Type": "application/json",
    },
    method: "HTTP methods",
    response: {
      "200": {
        Result: "string",
        Cached: "boolean",
        "Max Retry Exeeded": "boolean",
        "Request Id": "string",
      },

      "400": {
        Error: "string",
        "Request Id": "string",
      },
      "500": {
        Error: "string",
        "Request Id": "string",
      },
    },
  },
  {
    name: "Reject",
    id: 3,
    url: "http://something",
    version: "1.0",
    requestbody: {
      declaredName: "source",
      panNumber: "source",
    },
    header: {
      attribute: "source", // attributes will be defined in master
    },
    method: "HTTP methods",
    response: {
      // expected schema for validation
      "[statusCode]": {
        attribute: "schema", // attributes will be defined in master
      },
    },
  },
];


export class BodyWidget extends React.Component<BodyWidgetProps> {
  state = {
    renderedPorts: null,
    inputParams: null,
    renderedPage: null,
    utilityInputParams: null,
    renderedApiCall:null,
  };
  cloneSelected() {
    let model = this.props.app.getActiveDiagram();
    let offset = { x: 100, y: 100 };
    let itemMap = {};
    _.forEach(model.getSelectedEntities(), (item: BaseModel<any>) => {
      let newItem = item.clone(itemMap);

      // offset the nodes slightly
      if (newItem instanceof NodeModel) {
        newItem.setPosition(
          newItem.getX() + offset.x,
          newItem.getY() + offset.y
        );
        model.addNode(newItem);
      } else if (newItem instanceof LinkModel) {
        // offset the link points
        newItem.getPoints().forEach((p) => {
          p.setPosition(p.getX() + offset.x, p.getY() + offset.y);
        });
        model.addLink(newItem);
      }
      (newItem as BaseModel).setSelected(false);
    });
    this.forceUpdate();
  }

  InPortAddition() {
    let model = this.props.app.getActiveDiagram();

    _.forEach(model.getSelectedEntities(), (item: BaseModel<any>) => {
      if (item instanceof DefaultNodeModel) {
        item.addInPort(`In-${item.getInPorts().length + 1}`, true);
        // console.log(port.serialize().extras);
      }
    });

    this.props.app.getDiagramEngine().repaintCanvas();
  }
  InPortDeletion() {
    let model = this.props.app.getActiveDiagram();

    _.forEach(model.getSelectedEntities(), (item: BaseModel<any>) => {
      if (item instanceof DefaultNodeModel) {
        if (item.getInPorts().length) {
          item.removePort(item.getInPorts()[item.getInPorts().length - 1]);
        }
      }
    });

    this.props.app.getDiagramEngine().repaintCanvas();
  }

  OutPortAddition() {
    let model = this.props.app.getActiveDiagram();

    _.forEach(model.getSelectedEntities(), (item: BaseModel<any>) => {
      if (item instanceof DefaultNodeModel) {
        item.addOutPort(`Out-${item.getOutPorts().length + 1}`, true);
      }
    });

    this.props.app.getDiagramEngine().repaintCanvas();
  }
  OutPortDeletion() {
    let model = this.props.app.getActiveDiagram();

    _.forEach(model.getSelectedEntities(), (item: BaseModel<any>) => {
      if (item instanceof DefaultNodeModel) {
        if (item.getOutPorts().length) {
          item.removePort(item.getOutPorts()[item.getOutPorts().length - 1]);
        }
      }
    });

    this.props.app.getDiagramEngine().repaintCanvas();
  }

  // const makeNode = React.forwardRef((ref:any) => {
  //   console.log("hello")
  //   console.log(ref)
  //   return <div ref={ref}>Child1</div> 
  // }
  // );
  
  // addRow = (tableID: string) => {

  //   let table: HTMLElement| null =document.getElementById(tableID);

  //   let rowCount = table.getAttributeNode.length
  //   let row = table.elementinsertRow(rowCount);

  //   let cell1 = row.insertCell(0);
  //   let element1 = document.createElement("input");
  //   element1.type = "text";
  //   element1.name = "key";
  //   element1.placeholder = "Enter key"
  //   cell1.appendChild(element1);

  //   let cell2 = row.insertCell(1);
  //   let element2 = document.createElement("input");
  //   element2.type = "text";
  //   element2.name = "value";
  //   element2.placeholder = "Enter Value"
  //   cell2.appendChild(element2);
  //   return("Jellele")

  // };

  // deleteRow = (tableID) => {
  //     var table:HTMLTableElement =<HTMLTableElement> document.getElementById(tableID)</HTMLTableElement>
  //     let rowCount = table.rows.length;
  //     if (rowCount) {
  //       table.deleteRow(rowCount - 1);
  //     }
  // };

  buildScreen = (e:any) => {
    console.log("HIIIIIIIIIIIIIII")
    console.log(e.target[0].value)
  }

  renderPage = (item: any = null) => {

    
    let obj = item.getOptions().extras.data;
    let keys = Object.keys(obj)
    console.log("STARTTTTTT")
    let formItems=obj[keys[3]]
    // let   
    let i = Object.keys(formItems)
    console.log(i)
    let z=0
    // let count = -1
    
    // i.map((key: any) => {
    //   console.log("HEYAAAAA")
    //   console.log(formItems[key])
    //   let element = Object.keys(formItems[key])
    //   console.log("element",element)
    // })
  
      
    // console.log("HEYYYYYY");
    return (
      <React.Fragment>
        <Tray style={{ backgroundColor: "green" }}>
          <label>Select Outports:</label>
          <p>
            
          </p>
          <FlavorForm key={z++} app={this.props.app} obj={obj} item={item}></FlavorForm>
          <form onSubmit={(e) => {
            e.preventDefault()
            this.buildScreen(e)
          }}>
            </form> 
            
          </Tray>
      </React.Fragment>
    )
  }
  getDataFromPreviousNode = (node: DefaultNodeModel) => {
    let reqPort = node.getInPorts()[0];
    let l1 = reqPort.getLinks();
    let l2 = _.values(l1)[0];
    console.log(l2);
    if (l2) {
      let p1 = l2.getSourcePort();
      let n1 = p1.getParent();
      return n1.getOptions().extras.fields;
    }
  };

  getFieldsObject = (data: string[]) => {
    let obj: { [key: string]: Object } = {};
    _.map(data, (i) => {
      obj[i] = { label: i, type: "text" };
    });
    console.log(obj);
    return obj;
  };   


  renderApiCall = (item: any = null) => { 
    let obj = item.getOptions().extras.data;
    let keys = Object.keys(obj)
    console.log("STARTTTTTT")
    console.log(obj, keys)
    
    return (
      <React.Fragment>
        <ApiForm app={this.props.app} dataForApi={dataFromApi} item={item}></ApiForm>
      </React.Fragment>
    )
  }














































  

  renderInputs = (item: any = null) => {
    let obj = item.getOptions().extras.data.requestbody;
    let params = Object.keys(obj);
    return (
      <React.Fragment>
        {params.length > 0 ? (
          <Tray style={{ backgroundColor: "green" }}>
            {params.map((param: any) => (
              <React.Fragment key={param}>
                <p>{param}</p>
                <input
                  name={param}
                  type={"text"}
                  defaultValue={obj[param]}
                  placeholder={"source"}
                  onChange={(e) => {
                    item.getOptions().extras.data.requestbody[param] =
                      e.target.value;
                  }}
                />
              </React.Fragment>
            ))}
          </Tray>
        ) : null}
      </React.Fragment>
    );
  };
  renderPorts = (item: any = null) => {
    // for decision nodes
    let outPorts: any = item.getOutPorts();
    item.getOptions().extras.fields = this.getDataFromPreviousNode(item);
    console.log(item);
    let fields = this.getFieldsObject(item.getOptions().extras.fields);
    console.log("line to be seen");
    console.log(fields);
    return (
      <React.Fragment>
        {outPorts.length > 0 ? (
          <Tray style={{ backgroundColor: "green" }}>
            {outPorts.map((port: any) => (
              <React.Fragment key={port.options.name}>
                <p>{port.options.name}</p>
                <QueryBuilder port={port} fields={fields} />
              </React.Fragment>
            ))}
          </Tray>
        ) : null}
      </React.Fragment>
    );
  };

  getKeysForFields = (
    dict: { [key: string]: any },
    parent: string | null = null
  ): Array<string> => {
    const keyList = [];
    for (const key in dict) {
      if (dict[key] !== undefined) {
        if (typeof dict[key] !== "object") {
          keyList.push(parent ? `${parent}.${key}` : key);
        } else if (typeof dict[key] === "object") {
          keyList.push(
            ...this.getKeysForFields(
              dict[key],
              parent ? `${parent}.${key}` : key
            )
          );
        }
      }
    }
    return keyList;
  };

  render() {
    this.props.app
      .getDiagramEngine()
      .getActionEventBus()
      .registerAction(new DeleteItemsAction({ keyCodes: [46] }));
    let models = this.props.app.getActiveDiagram().getModels();
    console.log(models);
    models.forEach((item) => {
      if (item instanceof DefaultNodeModel) {
        if (item.getOptions().extras.data.type === "api") {
          item.registerListener({
            selectionChanged: (event: any) => {
              if (event.isSelected) {
                this.setState({
                  inputParams: this.renderInputs(item),
                  utilityInputParams: null,
                  renderedPorts: null,
                });
              } else {
                this.setState({
                  utilityInputParams: null,
                  renderedPorts: null,
                  inputParams: null,
                });
              }
            },
          });
        } else if (item.getOptions().extras.data.type === "decision") {
          item.registerListener({
            selectionChanged: (event: any) => {
              if (event.isSelected) {
                this.setState({
                  renderedPorts: this.renderPorts(item),
                  utilityInputParams: null,
                  inputParams: null,
                });
              } else {
                this.setState({
                  utilityInputParams: null,
                  inputParams: null,
                  renderedPorts: null,
                });
              }
            },
          });
        }
         else if (item.getOptions().extras.data.type === "pages") {
          item.registerListener({
            selectionChanged: (event: any) => {
              if (event.isSelected) {
                this.setState({
                  renderedPage: this.renderPage(item),
                });
              } else {
                this.setState({
                  renderedPage: null,
                });
              }
            },
          });
        }
        else if (item.getOptions().extras.data.type === "api_call") {
          item.registerListener({
            selectionChanged: (event: any) => {
              if (event.isSelected) {
                this.setState({
                  renderedApiCall: this.renderApiCall(item),
                });
              } else {
                this.setState({
                  renderedApiCall: null,
                });
              }
            },
          });
        }
      }
    });

    return (
      <Body>
        <Header>
          <div className="title">ABFL Diagram - Drag and Drop</div>
        </Header>

        <Content>
          <TrayWidget>
            <h2 style={{ paddingLeft: 10, color: "white" }}> Pages</h2>
            {dataforPages["screens"].map((item) => (
              <TrayItemWidget
                key={item.name}
                model={{ type: "pages", color: "rgb(192, 255, 0)", ...item }}
                name={item.name}
                color="rgb(192,255,0)"
              />
            ))}
            {/* <h2 style={{ paddingLeft: 10, color: "white" }}>Api Nodes</h2>
            {dataFromApi.map((item) => (
              <TrayItemWidget
                key={item.name}
                model={{ type: "api", color: "rgb(192, 255, 0)", ...item }}
                name={item.name}
                color="rgb(192,255,0)"
              />
            ))} */}
            <h2 style={{ paddingLeft: 10, color: "white" }}>Decision Nodes</h2>
            <TrayItemWidget
              model={{
                type: "decision",
                color: "rgb(7,95,45)",
                name: "Decision Node",
              }}
              name="Decision Node"
              color="rgb(7,95,45)"
            />

            <h2 style={{ paddingLeft: 10, color: "white" }}>Actions</h2>
            <TrayItemWidget
              model={{ type: "navigate", name: "Navigate To" }}
              name="Navigate"
              color="rgb(7,95,45)"
            />
            <TrayItemWidget
              model={{ type: "redirect", name: "Redirect To" }}
              name="Redirect"
              color="rgb(7,95,45)"
            />
            <TrayItemWidget
              model={{ type: "api_call", name: "Call API" ,api:"", headers: "",requestBody:""}}
              name="API Call"
              color="rgb(7,95,45)"
            />
            <TrayItemWidget
              model={{ type: "action", name: "Store Data" }}
              name="Data Store"
              color="rgb(7,95,45)"
            />
            <TrayItemWidget
              model={{ type: "action", name: "WorkFlow API" }}
              name="WorkFlow API"
              color="rgb(7,95,45)"
            />
            <TrayItemWidget
              model={{ type: "action", name: "Read From Source" }}
              name="Read From Source"
              color="rgb(7,95,45)"
            />
          </TrayWidget>

          <Layer
            onDrop={(event) => {
              var data = JSON.parse(
                event.dataTransfer.getData("storm-diagram-node")
              );

              console.log(data)

              var node: DefaultNodeModel | null = null;
              node = new DefaultNodeModel(data.name, data.color);
              // var pageString =JSON.stringify(pageJson)
              // var page = JSON.parse(pageString)
              // if (data.name in page) {
              //   console.log(page[data.name])
              //   for (var outport in page[data.name]["outport"]) {
              //     console.log(page[data.name]["outport"][outport])
              //     node.addOutPort(page[data.name]["outport"][outport]);
              //   }
              // }
              if (data.type !== "start") {
                node.addInPort("IN");
              }
              if (data.type == "navigate" || data.type == "redirect") {
                node.addOutPort("to");
              }

              if (node instanceof NodeModel) {
                node.getOptions().extras = {};
                node.getOptions().extras.data = data;
              }
              if (node.getOptions().extras.data.type === "api") {
                node.getOptions().id = data.name;
                console.log(node.getOptions().extras.data.type);
                node.getOptions().extras.fields = this.getKeysForFields(
                  data.response
                );
                let resObj = node.getOptions().extras.data.response;
                let resObjKeys = _.keys(resObj);
                console.log(resObjKeys);
                resObjKeys.map((status) => {
                  if (node instanceof NodeModel) {
                    node.addOutPort(status);
                  }
                  // return;
                });
              }
              var point = this.props.app
                .getDiagramEngine()
                .getRelativeMousePoint(event);
              node.setPosition(point);
              this.props.app.getDiagramEngine().getModel().addNode(node);
              this.forceUpdate();
            }}
            onDragOver={(event) => {
              event.preventDefault();
            }}
          >
            <Toolbar>
              <DemoButton
                onClick={() => {
                  const replacer: any = null; // used for avoiding TS error
                  console.log(
                    beautify(
                      this.props.app.getActiveDiagram().serialize(),
                      replacer,
                      2,
                      80
                    )
                  );
                }}
              >
                Serialize
              </DemoButton>
              <DemoButton
                onClick={() => {
                  this.cloneSelected();
                }}
              >
                Clone
              </DemoButton>
              <DemoButton
                onClick={() => {
                  this.InPortAddition();
                }}
              >
                Add InPort
              </DemoButton>
              <DemoButton onClick={this.InPortDeletion.bind(this)}>
                Remove InPort
              </DemoButton>
              <DemoButton onClick={this.OutPortAddition.bind(this)}>
                Add OutPort
              </DemoButton>
              <DemoButton onClick={this.OutPortDeletion.bind(this)}>
                Remove OutPort
              </DemoButton>
            </Toolbar>
            <Container
              background={"rgb(60, 60, 60)"}
              color={"rgba(255,255,255, 0.05)"}
            >
              <CanvasWidget engine={this.props.app.getDiagramEngine()} />
            </Container>
          </Layer>
          {this.state.renderedPorts}
          {this.state.inputParams}
          {this.state.renderedPage}
          {this.state.renderedApiCall}
        </Content>
      </Body>
    );
  }
}

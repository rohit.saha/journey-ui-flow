import * as React from "react";
import { Application } from "../Application";
import * as _ from "lodash";
import {
  DefaultNodeModel,
  LinkModel,
  NodeModel,
} from "@projectstorm/react-diagrams";
import {
  BaseModel,
  CanvasWidget,
  DeleteItemsAction,
} from "@projectstorm/react-canvas-core";
import { Header } from "../BodyWidget";
import { timeStamp } from "console";


export interface ApiFormProps {
  app: Application;
  obj?: any;
  item: any;
  dataForApi?: any;
}


export default class ApiForm extends React.Component<ApiFormProps> {
  state = {
    value: "",
    api: "",
    header: [],
    requestBody:[],
  }

  constructor(props: any) {
    super(props);
    // if (this.state.header!== []) {
    //   console.log("YES IT HAS")
    //   this.state = { value: 'coconut', api: this.props.item.getOptions().extras.data.api, header: this.props.item.getOptions().extras.data.header,requestBody:this.props.item.getOptions().extras.data.requestBody }
        
    // }
    // else {
    if (this.props.item.getOptions().extras.data.api !== "")
    {
      this.state= {value:'coconut',api:this.props.item.getOptions().extras.data.api,header:this.props.item.getOptions().extras.data.header,requestBody:this.props.item.getOptions().extras.data.requestBody}
      }
    
    else  
      this.state = { value: 'coconut', api: "", header: [], requestBody: [] }
    // }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    // this.addOutportJson = this.addOutportJson.bind(this);
  }
  
  handleChange(event: any) {
    
    if (event.target.value !== "---") {
      console.log("CHANGED")
      this.setState({ api: event.target.value, header: this.props.dataForApi[event.target.value].header,requestBody:this.props.dataForApi[event.target.value].requestbody })
    }    
    console.log(event.target.value)
  }
  

  handleSubmit(event: any) {
    // alert('Your favorite flavor is: ' + this.state.value);
    console.log("HIIIIII",this.state.api)
    // this.setState({header:this.props.item.getOptions().extras.data.header})
    // this.setState({requestBody:this.props.item.getOptions().extras.data.requestBody})
    
    // console.log(this.props.dataForApi[this.state.api].name)
    // this.setState({ apiJson: this.props.dataForApi[this.state.api].name })
    let model = this.props.app.getActiveDiagram();

    _.forEach(model.getSelectedEntities(), (item: BaseModel<any>) => {
      if (item instanceof DefaultNodeModel) {
        if(item.getOutPorts().length===1)
          item.removePort(item.getOutPorts()[item.getOutPorts().length - 1]);
        item.addOutPort(this.props.dataForApi[this.state.api].name, true);
        // console.log(port.serialize().extras);
      }

      if (item instanceof NodeModel) {
        
        item.getOptions().extras.data["requestBody"] = this.state.requestBody;
        item.getOptions().extras.data["api"] = this.state.api
        item.getOptions().extras.data["header"] =this.state.header
      }
      console.log("THIS----->",item)
    });
    this.props.app.getDiagramEngine().repaintCanvas();
    event.preventDefault();
  }

  render() {
    let obj= this.props.dataForApi
    let keys = Object.keys(obj)
    console.log(this.state.api,"hellooo", this.state.header,this.state.requestBody)
    console.log(keys, obj)
    let count=0
    
    let headers:any
    let headerKeys: any
    let request:any 
    let requestKeys:any
    console.log("HERRRRRE",this.state.api)

    if (this.state.api !== "")
    {
      headers = this.state.header
      headerKeys =Object.keys(headers)
      console.log(headers, headerKeys)
      request = this.state.requestBody
      requestKeys = Object.keys(request)
      console.log(request,requestKeys)
    }


    if (this.state.api === "") {
      return (
        <React.Fragment>
          <form onSubmit={this.handleSubmit}>
            <label>Select API:</label>
            <select value={this.state.api} onChange={this.handleChange}>
              <option value="---">---</option>
              {this.props.dataForApi.map((api: any) => {
                return (
                  <React.Fragment>
                    <option value={count++}>{api.name}</option>
                  </React.Fragment>
                )
              })}
            </select>
          </form>
        </React.Fragment>

      
      )
    }
    else {
      return (
        <React.Fragment>
          <form onSubmit={this.handleSubmit}>
            <label>Select API:</label>
            <select value={this.state.api} onChange={this.handleChange}>
              <option value="---">---</option>
              {this.props.dataForApi.map((api: any) => {
              
                return (
                  <React.Fragment>
                    <option value={count++}>{api.name}</option>
                  </React.Fragment>
                )
              })}
            </select>
            <p></p>
            <header><b>Headers</b></header><p></p>
            {headerKeys.map((no:any) => {
              return (
                <div>
                <label><i><b>{no}</b></i></label><br></br>
                  <input type="text" placeholder={headers[no]} defaultValue={this.state.header[no]} onChange={(e) => {
                    // console.log(this.props.item.getOptions().extras.data)
                    // this.props.item.getOptions().extras.data.header = {}
                    // this.props.item.getOptions().extras.data.header[no]=e.target.value
                    // this.setState({header:this.props.item.getOptions().extras.data.header})
                    //   e.target.value;
                    
                    let ans=headers
                    ans[no]=e.target.value
                    this.setState({ header: ans })
                    console.log(this.state.header)
                  }}/>
                </div>
              )
              
            })}
            <p></p>
            <header><b>Request Body</b></header><p></p>
            {requestKeys.map((no: any) => {
                  return (
                    <div>
                      <label><i><b>{no}</b></i></label><br></br>
                      <input type="text" placeholder={request[no]} defaultValue={this.state.requestBody[no]} onChange={(e) => {
                        // this.props.item.getOptions().extras.data.requestBody = {}
                        // this.props.item.getOptions().extras.data.requestBody[no] =
                        //   e.target.value;
                        let ans=request
                        ans[no]=e.target.value
                        this.setState({ requestBody: ans })
                        console.log(this.state.requestBody)
                      }}>
                      </input>
                    </div>
                  )
                })   
            }
            
            
            {/* {console.log(obj[this.state.api].header)} */}
            <p></p>
            <input type="submit" value="Submit"></input>
          </form>
        </React.Fragment>
      )
    }
  }
}
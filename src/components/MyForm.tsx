import * as React from "react";
import { Application } from "../Application";
import * as _ from "lodash";
import MyScreenItems from "./MyScreenItems";
import {
  DefaultNodeModel,
  LinkModel,
  NodeModel,
} from "@projectstorm/react-diagrams";
import {
  BaseModel,
  CanvasWidget,
  DeleteItemsAction,
} from "@projectstorm/react-canvas-core";

export interface FlavorFormProps {
  app: Application;
  obj: any;
  item?: any;
}

export default class FlavorForm extends React.Component<FlavorFormProps> {
  state = {
    value: " ",
    outports: [],
    outportJson: [],
  }

  constructor(props:any) {
    super(props);
    if ("final" in this.props.item.getOptions().extras.data) {
      console.log("YES IT HAS")
      this.state = { value: 'coconut', outports: this.props.item.getOptions().extras.data.outports, outportJson: this.props.item.getOptions().extras.data.final }
      
    }
    else {
      this.state = { value: 'coconut', outports: [], outportJson: [] }
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.addOutportJson = this.addOutportJson.bind(this);
  }

  handleChange(event: any) {
    this.setState({ value: "---" });
    if (event.target.value !== "---") {
      let items: Array<any> = this.state.outports
      items.push(event.target.value)
      console.log(items)
    }
  }

  handleSubmit(event:any) {
    // alert('Your favorite flavor is: ' + this.state.value);
    console.log(this.state.outportJson)
    let model = this.props.app.getActiveDiagram();

    _.forEach(model.getSelectedEntities(), (item: BaseModel<any>) => {
      if (item instanceof DefaultNodeModel) {
        while (item.getOutPorts().length) {
          item.removePort(item.getOutPorts()[item.getOutPorts().length - 1]);
        }
        this.state.outportJson.forEach((outport) => {
          
          item.addOutPort(outport["name"], true);
        })
        // console.log(port.serialize().extras);
      }

      if (item instanceof NodeModel) {
        
        item.getOptions().extras.data["final"] = this.state.outportJson;
        item.getOptions().extras.data["outports"]= this.state.outports
      }
      console.log(item)
    });

    


    this.props.app.getDiagramEngine().repaintCanvas();
    event.preventDefault();
  }

  addOutportJson(item: any) {
    let items: Array<any> = this.state.outportJson
    if (this.state.outportJson.length < item['id'] + 1)
    {
      let items: Array<any> = this.state.outportJson
      items.push(item)
    }
    else
    {
      items[item['id']]=item
      }
    console.log("hejcacacmlamca",this.state.outportJson)
 }

  render() {
    let obj= this.props.obj
    let keys = Object.keys(obj)
    console.log("Inside FlavourForm")
    let formItems=obj[keys[3]]
    // let   
    let i = Object.keys(formItems)
    console.log(i)
    let j=0
    let count=0

    return (
      
      <form onSubmit={this.handleSubmit}>
        <label>
          Add Outports:
          <select value={this.state.value} onChange={this.handleChange}>
          <option value="---">---</option>
          {
            
            i.map((key: any) => {
              // console.log("HOMEEEEEEE")
              // console.log(formItems[key]);
              
              return (
                <React.Fragment>
                  <option value={ formItems[key].name+"::"+formItems[key].type }>{formItems[key].name}</option>
                  
                  </React.Fragment>
              )
  
              })
              }
            {/* <option value="grapefruit">Grapefruit</option>
            <option value="lime">Lime</option>
            <option value="coconut">Coconut</option>
            <option value="mango">Mango</option> */}
          </select>
        </label>
        {
          
          this.state.outports.map((outport) => {
            // console.log(outport)
            // let x = this.state.outportJson
            // obj = JSON.parse(JSON.stringify(x))
            // obj[count] = ""
            
            // this.setState({ outportJson: JSON.stringify(obj)})
            let x= this.props.item.getOptions().extras.data
            if ("final" in x && x.final.length>count) {
              return (

                <MyScreenItems id={count++} name={outport} onChangeProps={this.addOutportJson} onEvent={x.final[j].onEvent} action={x.final[j++].action}/>
              );
            }
            else {
              return (

                <MyScreenItems id={count++} name={outport} onChangeProps={this.addOutportJson}/>
              );
            }
          })
          
        }
        <input type="submit" value="Submit" />
      </form> 
    );
  }
}

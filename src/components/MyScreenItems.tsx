import React, { Component } from 'react'

export interface MyScreenItemsProps {
  id: any;
  name: any;
  onChangeProps?: any;
  onEvent?: any,
  action?:any,
}


export class MyScreenItems extends Component<MyScreenItemsProps> {
  state = {
    onEvent: "",
  }
  constructor(props:any) {
    super(props);
    if ("onEvent" in this.props)
      {console.log("YESSSSS")
      this.state = { onEvent: `${this.props.onEvent}` };
    }
    else  
      this.state = {onEvent:"---"};
    console.log(this.state)
    
    this.setEvent = this.setEvent.bind(this);
  }

  setEvent(event: any) {
    if (event.target.value !== "---") {
      this.setState({ onEvent: event.target.value })
      console.log(this.props.id)
      this.props.onChangeProps({ id: this.props.id, name: this.props.name, onEvent: event.target.value})
    }
  }

  // setAction(event: any) {
  //   if (event.target.value !== "---") {
  //     this.setState({ action: event.target.value })
  //     this.props.onChangeProps({id:this.props.id,name:this.props.name, onEvent: this.state.onEvent,action:event.target.value})    
  //   }
  //   }
  render() {
    
    let a = this.props.name
    console.log(a,this.props.id)
    return (
      <div>
        <label>{a}</label>
        <select value={this.state.onEvent} onChange={this.setEvent}>
        <option value="---">---</option>
        <option value="onClick">onClick</option>
        <option value="onChange">onChange</option>
        <option value="onBlur">onBlur</option>
        <option value="onFocus">onFocus</option>
        <option value="onSubmit">onSubmit</option>
        </select>
        {/* 
        <select value={this.state.action} onChange={this.setAction}>
        <option value="---">---</option>
        <option value="NAVIGATE">NAVIGATE</option>
        <option value="API_CALL">API_CALL</option>
        <option value="WORKFLOW_API_CALL">WORKFLOW_API</option>
        <option value="STORE_DATA">STORE_DATA</option>
        <option value="READ_FROM_SOURCE">READ_FROM_SOURCE</option>

        </select> */}
      </div>
    )
  }
}

export default MyScreenItems

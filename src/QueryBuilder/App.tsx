import React, { Component } from "react";
import {
  Query,
  Builder,
  Utils as QbUtils,
  Config,
  //   BasicConfig,
  JsonGroup,
  BuilderProps,
  ImmutableTree,
} from "react-awesome-query-builder";
import AntdConfig from "react-awesome-query-builder/lib/config/antd";
import "antd/dist/antd.css";
import "react-awesome-query-builder/css/styles.scss";
const InitialConfig: Config = AntdConfig; // or BasicConfig

// const config: Config = {
//   ...InitialConfig,
//   fields: {
//     qty: {
//       label: "Qty",
//       type: "number",
//       fieldSettings: {
//         min: 0,
//       },
//       valueSources: ["value"],
//       preferWidgets: ["number"],
//     },
//     price: {
//       label: "price",
//       type: "number",
//       valueSources: ["value"],
//       fieldSettings: {
//         min: 0,
//         max: 100,
//       },
//       preferWidgets: ["slider", "rangeslider"],
//     },
//     color: {
//       label: "color",
//       type: "select",
//       valueSources: ["value"],
//       listValues: [
//         { value: "yellow", title: "Yellow" },
//         { value: "green", title: "Green" },
//         { value: "orange", title: "Orange" },
//       ],
//     },
//     is_promotion: {
//       label: "Promo?",
//       type: "boolean",
//       operators: ["equal"],
//       valueSources: ["value"],
//     },
//   },
// };

const queryValue: JsonGroup = { id: QbUtils.uuid(), type: "group" };
// const queryValue: JsonGroup = {
//   id: "a898ab9a-0123-4456-b89a-b172ea1edf53",
//   type: "group",
//   children1: {
//     "aab8898a-cdef-4012-b456-7172ea1efadb": {
//       type: "rule",
//       properties: {
//         field: "qty",
//         operator: "equal",
//         value: [77],
//         valueSrc: ["value"],
//         valueType: ["number"],
//       },
//     },
//   },
// };

export default class QueryBuilder extends Component<{
  port: any;
  fields: any;
}> {
  config = { ...InitialConfig, fields: this.props.fields };
  state = {
    config: this.config,
    tree: this.props.port.options.hasOwnProperty("extras")
      ? this.props.port.options.extras.hasOwnProperty("tree")
        ? QbUtils.checkTree(
            QbUtils.loadTree(JSON.parse(this.props.port.options.extras.tree)),
            this.config
          )
        : QbUtils.checkTree(QbUtils.loadTree(queryValue), this.config)
      : QbUtils.checkTree(QbUtils.loadTree(queryValue), this.config),
  };

  render = () => {
    try {
      console.log(JSON.parse(this.props.port.options.extras.tree));
    } catch {
      console.log("Not Found");
    }
    console.log(this.props.fields);

    let results = this.renderResult(this.state);

    return (
      <div>
        <Query
          {...this.config}
          value={this.state.tree}
          onChange={this.onChange}
          renderBuilder={this.renderBuilder}
        />
        {/* {results} */}
      </div>
    );
  };
  renderBuilder = (props: BuilderProps) => (
    <div className="query-builder-container" style={{ padding: "10px" }}>
      <div className="query-builder qb-lite">
        <Builder {...props} />
      </div>
    </div>
  );

  renderResult = ({
    tree: immutableTree,
    config,
  }: {
    tree: ImmutableTree;
    config: Config;
  }) => {
    return (
      <div className="query-builder-result">
        <div>
          Query string:{" "}
          <pre>
            {JSON.stringify(QbUtils.queryString(immutableTree, config))}
          </pre>
        </div>
        {/* <div>
          MongoDb query:{" "}
          <pre>
            {JSON.stringify(QbUtils.mongodbFormat(immutableTree, config))}
          </pre>
        </div>
        <div>
          SQL where:{" "}
          <pre>{JSON.stringify(QbUtils.sqlFormat(immutableTree, config))}</pre>
        </div> */}
        <div>
          JsonLogic:{" "}
          <pre>
            {JSON.stringify(QbUtils.jsonLogicFormat(immutableTree, config))}
          </pre>
        </div>
        <div>
          JsonTree: <pre>{JSON.stringify(QbUtils.getTree(immutableTree))}</pre>
        </div>
      </div>
    );
  };

  onChange = (immutableTree: ImmutableTree, config: Config) => {
    this.setState({ tree: immutableTree, config: config });

    const jsonTree = QbUtils.getTree(immutableTree);

    this.props.port.options.extras = {
      condition: QbUtils.jsonLogicFormat(immutableTree, config),
      tree: JSON.stringify(jsonTree),
    };
    // console.log(JSON.stringify(jsonTree));
    console.log(jsonTree);
  };
}
